const DAY = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu']
const MONTH = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember']
const TODAY = new Date();

function getAllInput() {
    const input = {
        TODAY_DAY: DAY[TODAY.getDay()],
        TODAY_DATE: `${TODAY.getDate()} ${MONTH[TODAY.getMonth()]} ${TODAY.getFullYear()}`,
        TENANT_NAME: document.getElementById('tenantName').value,
        TENANT_ADDRESS: document.getElementById('tenantAddress').value,
        TENANT_ID: document.getElementById('tenantId').value,
        UNIT_TYPE: document.getElementById('unitType').value,
        APARTMENT_NAME: document.getElementById('apartmentName').value,
        APARTMENT_ADDRESS: document.getElementById('apartmentAddress').value,
        RENT_PERIOD: document.getElementById('rentPeriod').value,
        RENT_START_DATE: document.getElementById('rentStartDate').value,
        RENT_END_DATE: document.getElementById('rentEndDate').value,
        RENT_PRICE: document.getElementById('rentPrice').value,
        RENT_DEPOSIT: document.getElementById('rentDeposit').value,
        RENT_FINE: document.getElementById('rentFine').value,
        YUKSTAY_NAME_IN_BANK: document.getElementById('yukstayNameInBank').value,
        YUKSTAY_BANK_NAME: document.getElementById('yukstayBankName').value,
        YUKSTAY_BANK_NO: document.getElementById('yukstayBankNo').value
    }
    return input;
}

function setAllInput(input) {
    document.getElementById('TENANT_NAME').innerHTML = input.TENANT_NAME;
    document.getElementById('TENANT_ADDRESS').innerHTML = input.TENANT_ADDRESS;
    document.getElementById('TENANT_ID').innerHTML = input.TENANT_ID;
    document.getElementById('UNIT_TYPE').innerHTML = input.UNIT_TYPE;
    document.getElementById('APARTMENT_NAME').innerHTML = input.APARTMENT_NAME;
    document.getElementById('APARTMENT_ADDRESS').innerHTML = input.APARTMENT_ADDRESS;
    document.getElementById('RENT_PERIOD').innerHTML = input.RENT_PERIOD;
    document.getElementById('RENT_START_DATE').innerHTML = input.RENT_START_DATE;
    document.getElementById('RENT_END_DATE').innerHTML = input.RENT_END_DATE;
    document.getElementById('RENT_PRICE').innerHTML = input.RENT_PRICE;
    document.getElementById('RENT_DEPOSIT').innerHTML = input.RENT_DEPOSIT;
    document.getElementById('RENT_FINE').innerHTML = input.RENT_FINE;
    document.getElementById('YUKSTAY_NAME_IN_BANK').innerHTML = input.YUKSTAY_NAME_IN_BANK;
    document.getElementById('YUKSTAY_BANK_NAME').innerHTML = input.YUKSTAY_BANK_NAME;
    document.getElementById('YUKSTAY_BANK_NO').innerHTML = input.YUKSTAY_BANK_NO;
}

function clearAllInput() {
    document.getElementById('tenantName').value = null;
    document.getElementById('tenantAddress').value = null;
    document.getElementById('tenantId').value = null;
    document.getElementById('unitType').value = null;
    document.getElementById('apartmentName').value = null;
    document.getElementById('apartmentAddress').value = null;
    document.getElementById('rentPeriod').value = null;
    document.getElementById('rentStartDate').value = null;
    document.getElementById('rentEndDate').value = null;
    document.getElementById('rentPrice').value = null;
    document.getElementById('rentDeposit').value = null;
    document.getElementById('rentFine').value = null;
    document.getElementById('yukstayNameInBank').value = null;
    document.getElementById('yukstayBankName').value = null;
    document.getElementById('yukstayBankNo').value = null;
}

function submitForm() {
    setAllInput(getAllInput());

    document.getElementById("form_contract").style.display = "none";
    document.getElementById("contract").removeAttribute("style");

    document.styleSheets[0].disabled = true;
}

function submitContract() {
    if (!document.getElementById('contract-agreement-checkbox').checked) {
        return alert('Please read and agree on the contract first.');
    }

    document.getElementById('contract-agreement-checkbox').checked = false;
    document.getElementById("contract").style.display = "none";
    document.getElementById("form_contract").removeAttribute("style");
    document.styleSheets[0].disabled = false;
    clearAllInput();
    alert("Thank you for your contract agreement. The data has been saved to server.");
}
