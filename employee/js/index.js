const DAY = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu']
const MONTH = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember']
const TODAY = new Date();

function getAllInput() {
    const input = {
        TODAY_DAY: DAY[TODAY.getDay()],
        TODAY_DATE: `${TODAY.getDate()} ${MONTH[TODAY.getMonth()]} ${TODAY.getFullYear()}`,
        EMPLOYEE_NAME: document.getElementById('employeeName').value,
        EMPLOYEE_ID: document.getElementById('employeeId').value,
        START_WORKING_DATE: document.getElementById('startWorkingDate').value,
        JOB_TYPE: document.getElementById('jobType').value,
        JOB_TITLE: document.getElementById('jobTitle').value,
        JOB_TASKS: document.getElementById('jobTask').value,
        SALARY_NUMBER: document.getElementById('salarayNumber').value,
        SALARY_STRING: document.getElementById('salaryString').value
    }
    return input;
}

function setAllInput(input) {
    document.getElementById('TODAY_DAY').innerHTML = input.TODAY_DAY;
    document.getElementById('TODAY_DATE').innerHTML = input.TODAY_DATE;
    document.getElementById('EMPLOYEE_NAME').innerHTML = input.EMPLOYEE_NAME;
    document.getElementById('EMPLOYEE_ID').innerHTML = input.EMPLOYEE_ID;
    document.getElementById('START_WORKING_DATE').innerHTML = input.START_WORKING_DATE;
    document.getElementById('JOB_TYPE').innerHTML = input.JOB_TYPE;
    document.getElementById('JOB_TITLE').innerHTML = input.JOB_TITLE;
    document.getElementById('JOB_TASKS').innerHTML = input.JOB_TASKS;
    document.getElementById('SALARY_NUMBER').innerHTML = input.SALARY_NUMBER;
    document.getElementById('SALARY_STRING').innerHTML = input.SALARY_STRING;
}

function clearAllInput() {
    document.getElementById('employeeName').value = null;
    document.getElementById('employeeId').value = null;
    document.getElementById('startWorkingDate').value = null;
    document.getElementById('jobType').value = null;
    document.getElementById('jobTitle').value = null;
    document.getElementById('jobTask').value = null;
    document.getElementById('salarayNumber').value = null;
    document.getElementById('salaryString').value = null;
}

function submitForm() {
    setAllInput(getAllInput());

    document.getElementById("form_contract").style.display = "none";
    document.getElementById("contract").removeAttribute("style");

    document.styleSheets[0].disabled = true;
}

function submitContract() {
    if (!document.getElementById('contract-agreement-checkbox').checked) {
        return alert('Please read and agree on the contract first.');
    }

    document.getElementById('contract-agreement-checkbox').checked = false;
    document.getElementById("contract").style.display = "none";
    document.getElementById("form_contract").removeAttribute("style");
    document.styleSheets[0].disabled = false;
    clearAllInput();
    alert("Thank you for your contract agreement. The data has been saved to server.");
}
