const DAY = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu']
const MONTH = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember']
const TODAY = new Date();

function getAllInput() {
    const input = {
        TODAY_DAY: DAY[TODAY.getDay()],
        TODAY_DATE: `${TODAY.getDate()} ${MONTH[TODAY.getMonth()]} ${TODAY.getFullYear()}`,
        OWNER_NAME : document.getElementById('ownerName').value,
        OWNER_ADDRESS : document.getElementById('ownerAddress').value,
        OWNER_ID : document.getElementById('ownerId').value,
        UNIT_TYPE : document.getElementById('unitType').value,
        APARTMENT_NAME : document.getElementById('apartmentName').value,
        APARTMENT_ADDRESS : document.getElementById('apartmentAddress').value,
        NAME_IN_OWNER_BANK : document.getElementById('nameInOwnerBank').value,
        OWNER_BANK_NAME : document.getElementById('ownerBankName').value,
        OWNER_BANK_NO : document.getElementById('ownerBankNo').value
    }
    return input;
}

function setAllInput(input) {
    document.getElementById('TODAY_DAY').innerHTML = input.TODAY_DAY;
    document.getElementById('TODAY_DATE').innerHTML = input.TODAY_DATE;
    document.getElementById('OWNER_NAME').innerHTML = input.OWNER_NAME;
    document.getElementById('OWNER_ADDRESS').innerHTML = input.OWNER_ADDRESS;
    document.getElementById('OWNER_ID').innerHTML = input.OWNER_ID;
    document.getElementById('UNIT_TYPE').innerHTML = input.UNIT_TYPE;
    document.getElementById('APARTMENT_NAME').innerHTML = input.APARTMENT_NAME;
    document.getElementById('APARTMENT_ADDRESS').innerHTML = input.APARTMENT_ADDRESS;
    document.getElementById('NAME_IN_OWNER_BANK').innerHTML = input.NAME_IN_OWNER_BANK;
    document.getElementById('OWNER_BANK_NAME').innerHTML = input.OWNER_BANK_NAME;
    document.getElementById('OWNER_BANK_NO').innerHTML = input.OWNER_BANK_NO;
}

function clearAllInput() {
    document.getElementById('ownerName').value = null;
    document.getElementById('ownerAddress').value = null;
    document.getElementById('ownerId').value = null;
    document.getElementById('unitType').value = null;
    document.getElementById('apartmentName').value = null;
    document.getElementById('apartmentAddress').value = null;
    document.getElementById('nameInOwnerBank').value = null;
    document.getElementById('ownerBankName').value = null;
    document.getElementById('ownerBankNo').value = null;
}

function submitForm() {
    setAllInput(getAllInput());

    document.getElementById("form_contract").style.display = "none";
    document.getElementById("contract").removeAttribute("style");

    document.styleSheets[0].disabled = true;
}

function submitContract() {
    if (!document.getElementById('contract-agreement-checkbox').checked) {
        return alert('Please read and agree on the contract first.');
    }

    document.getElementById('contract-agreement-checkbox').checked = false;
    document.getElementById("contract").style.display = "none";
    document.getElementById("form_contract").removeAttribute("style");
    document.styleSheets[0].disabled = false;
    clearAllInput();
    alert("Thank you for your contract agreement. The data has been saved to server.");
}
