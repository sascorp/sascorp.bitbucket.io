# Agreement

Git repository for yukstay agreement.

### Project structure

```
.
├── README.md
└── employee
    ├── README.md
    ├── css
    │   ├── main.css
    │   └── tnc.css
    ├── employee_contract.html
    └── js
        └── index.js
```
